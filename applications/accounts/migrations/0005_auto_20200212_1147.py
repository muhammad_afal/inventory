# Generated by Django 3.0.2 on 2020-02-12 11:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20200212_1145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='purchaser',
            field=models.BooleanField(default=False),
        ),
    ]
