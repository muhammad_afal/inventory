# Generated by Django 3.0.2 on 2020-02-12 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='purchaser',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
