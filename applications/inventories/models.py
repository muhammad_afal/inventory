from django.conf import settings
from datetime import datetime

from django.core.mail import send_mail

from django.db import models
from django.utils.translation import ugettext as _

from django.db.models.signals import pre_save,post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from applications.accounts.models import User, Teams
from applications.accounts.models import TimeStampModel

class BinLocation(TimeStampModel):
    name = models.CharField(max_length=256,blank=True,null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Bin Location')
        verbose_name_plural = _('Bin Location')
        ordering = ('-created',)


class Projects(TimeStampModel):
    name = models.CharField(max_length=256,blank=True,null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Projects')
        verbose_name_plural = _('Projects')
        ordering = ('-created',)


class Shelves(TimeStampModel):
    name = models.CharField(max_length=256,blank=True,null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Shelves')
        verbose_name_plural = _('Shelves')
        ordering = ('-created',)


class Innerholes(TimeStampModel):
    name = models.CharField(max_length=256,blank=True,null=True)
    bin_obj = models.ForeignKey(BinLocation,related_name="bin_hole",null=True,blank=True,on_delete=models.CASCADE)
    shelf = models.ForeignKey(Shelves,related_name="shelf_obj",null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Inner holes')
        verbose_name_plural = _('Inner holes')
        ordering = ('-created',)

class Vendors(TimeStampModel):
    name = models.CharField(max_length=256,blank=True,null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Vendors')
        verbose_name_plural = _('Vendors')
        ordering = ('-created',)

class Product(TimeStampModel):
    part_number = models.CharField(max_length=256,null=True,blank=True)
    part_description = models.CharField(max_length=256,null=True,blank=True)
    part_name = models.CharField(max_length=256,null=True,blank=True)
    count = models.IntegerField(null=True,blank=True,default=0)
    stk_product_in = models.CharField(max_length=256,null=True,blank=True)

class ProductIn(Product):
    bin_in_location = models.ForeignKey(BinLocation,related_name="bin_in",null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.part_name

    class Meta:
        verbose_name = _('Product In')
        verbose_name_plural = _('Product In')
        ordering = ('-created',)


class ProductOut(Product):
    utilized_count = models.IntegerField(null=True, blank=True,default=0)
    # recieved_by = models.CharField(max_length=256,null=True, blank=True)
    recieved_date = models.DateField(null=True, blank=True)
    stk_product_out = models.CharField(max_length=256,null=True,blank=True)
    # project = models.CharField(max_length=256,null=True,blank=True)
    bin_out_locatiion = models.ForeignKey(BinLocation,related_name="bin_out",null=True,blank=True,on_delete=models.CASCADE)
    project = models.ForeignKey(Projects,related_name="project",null=True,blank=True,on_delete=models.CASCADE)
    team = models.ForeignKey(Teams,related_name="team",null=True,blank=True,on_delete=models.CASCADE)
    return_count = models.IntegerField(null=True,blank=True,default=0)
    person = models.ForeignKey(User, related_name="allocated_person", null= True, blank=True, on_delete=models.CASCADE)


    def __str__(self):
        return self.part_name

    class Meta:
        verbose_name = _('Product Out')
        verbose_name_plural = _('Product Out')
        ordering = ('-created',)


class ProductPurchase(Product):
    purchased_on = models.DateField(null=True, blank=True)
    purchaser = models.ForeignKey(User, related_name="purchase", null=True, blank=True, on_delete=models.CASCADE)
    # bin_purchase_location = models.ForeignKey(BinLocation,related_name="bin_purchase", null=True, blank=True, on_delete=models.CASCADE)
    vendor_name = models.ForeignKey(Vendors,related_name="vendor", null=True, blank=True,on_delete=models.CASCADE)
    location = models.ForeignKey(Innerholes,related_name="hole",null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return  self.part_name

    class Meta:
        verbose_name = _('Product Purchase')
        verbose_name_plural = _('Product Purchase')
        ordering = ('-created',)


class OverallInventory(Product):
    product_purchased_count = models.IntegerField(null=True,blank=True,default=0)
    product_in_count = models.IntegerField(null=True, blank=True,default=0)
    product_out_count = models.IntegerField(null=True,blank=True,default=0)
    product_returned_count = models.IntegerField(null=True,blank=True,default=0)
    total_count = models.IntegerField(null=True,blank=True,default=0)
    remarks = models.TextField(null=True,blank=True)
    location = models.ForeignKey(Innerholes,related_name='overall_location',blank=True,null=True,on_delete=models.CASCADE)
    stk = models.CharField(max_length=256,null=True,blank=True)


    def __str__(self):
        return self.part_name

    class Meta:
        verbose_name = _('Overall Inventory')
        verbose_name_plural = _('Overall Inventory')
        ordering = ('-created',)

class File(TimeStampModel):

    xl_file = models.FileField(upload_to="Excel Files",blank=True,null=True)


class ProjectInventory(Product):
    project = models.ManyToManyField(Projects, related_name="project_inventory", null=True, blank=True)
    available = models.ForeignKey(OverallInventory,related_name="overall", null=True, blank=True,on_delete=models.CASCADE)
    product_out = models.ForeignKey(ProductOut,related_name="out_product", null=True, blank=True,on_delete=models.SET_NULL)

    def __str__(self):
        return self.part_name

class InventoryReturned(Product):
    count_returned = models.IntegerField(null=True,blank=True,default=0)
    returned_by = models.CharField(max_length=256,null=True,blank=True)
    returned_date = models.DateField(null=True, blank=True)
    # project = models.CharField(max_length=256,null=True,blank=True)
    project = models.ForeignKey(Projects, related_name="project_return", null=True, blank=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Teams, related_name="team_return", null=True, blank=True, on_delete=models.CASCADE)
    person = models.ForeignKey(User, related_name="returned_person", null= True, blank=True, on_delete=models.CASCADE)
    out_object = models.ForeignKey(ProductOut,related_name="product_out", null=True, blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.part_name

    class Meta:
        verbose_name = _('Returned Inventory')
        verbose_name_plural = _('Returned Inventory')
        ordering = ('-created',)

CHOICES = (
    ("1", 'Pending'),
    ("2", 'Order Placed'),
    ("3", 'Delivered'),
   )


def number():
    no = PurchaseRequest.objects.count()
    if no == 0:
        return 1
    else:
        return no + 1



class PurchaseRequest(Product):
    requested_by = models.CharField(max_length=256,null=True,blank=True)
    request_number = models.IntegerField(null=True,blank=True,default=number)
    requested_on = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2,null=True, blank=True, choices=CHOICES)
    age = models.IntegerField(null=True,blank=True)
    project = models.ForeignKey(Projects,related_name="request_project",on_delete=models.CASCADE,null=True,blank=True)
    purchaser = models.ForeignKey(User,related_name="purchase_user",on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.requested_by + " " + self. part_name

    class Meta:
        verbose_name = _('Purchase Request')
        verbose_name_plural = _('Purchase Requests')
        ordering = ('-created',)

class ModelNames(TimeStampModel):
    model_name = models.CharField(max_length=256,null=True,blank=True)
    voltage = models.CharField(max_length=256,null=True,blank=True)
    item_code = models.CharField(max_length=20,null=True,blank=True)
    count_delivered = models.IntegerField(null=True,blank=True)
    date_of_manufacture = models.DateField(null=True, blank=True)
    remarks = models.CharField(max_length=256,null=True,blank=True)

    def __str__(self):
        return self.model_name

    class Meta:
        verbose_name = _('Model Names')
        verbose_name_plural = _('Model Names ')
        ordering = ('-created',)


class Accessories(TimeStampModel):
    name = models.CharField(max_length=256,null=True,blank=True)
    item_code = models.CharField(max_length=20,null=True,blank=True)
    model_names = models.ManyToManyField(ModelNames,related_name='models',null=True,blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Accessories')
        verbose_name_plural = _('Accessories')
        ordering = ('-created',)


@receiver(post_save,sender=OverallInventory)
def my_callback(sender, instance, *args, **kwargs):
    if instance.stk:
        instance.part_description = str(instance.part_description) + '&' + str(instance.stk)
    instance.total_count = instance.product_in_count - instance.product_out_count + instance.product_purchased_count + instance.product_returned_count

@receiver(post_save,sender=ProductIn)
def my_callback(sender, instance, *args, **kwargs):
    if instance.stk_product_in:
        instance.part_description = str(instance.part_description) + '&' + str(instance.stk_product_in)
    else:
        pass

@receiver(post_save, sender=PurchaseRequest)
def user_post_save(sender,instance, **kwargs):
    if kwargs['created']:
        message = render_to_string('request_confirmation_mail.html', {
            'today':datetime.today().strftime('%Y-%m-%d'),
            'part_name':instance.part_name,
            'part_description':instance.part_description,
            'count':instance.count,
            'request_no':instance.request_number,
            "requested_on":instance.requested_on,
            "status":instance.get_status_display(),
            "project":instance.project,
            "requested_by":instance.requested_by,
            "purchaser":instance.purchaser.first_name
        })
        print(instance.purchaser.first_name)
        to_email = [instance.requested_by,instance.purchaser.email]
        send_mail(
            'New Request Placed.',
            'RandD Inventory',
            'inventory.ustglobal@gmail.com',
            to_email,
            html_message=message, )
    else:
        pass


@receiver(post_save, sender=ProductPurchase)
def save_overall(sender, instance, **kwargs):
    try:
        obj = OverallInventory.objects.get(part_name=instance.part_name)
    except:
        obj = OverallInventory()
    obj.product_purchased_count = instance.count
    obj.part_name = instance.part_name
    obj.part_description = instance.part_description
    # obj.total_count = instance.count - obj.product_in_count - obj.product_out_count + obj.product_returned_count
    obj.total_count = instance.count
    obj.save()


