from .models import Projects,ModelNames,Accessories,OverallInventory

def projects(request):
    context = {
        "projects":Projects.objects.all().order_by('created'),
        "models":ModelNames.objects.all().order_by('created'),
        "accessories":Accessories.objects.all().order_by('created'),
        "item_descriptions":OverallInventory.objects.all().values("part_description")
    }
    return context
