# Generated by Django 3.0.2 on 2020-08-19 06:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventories', '0037_auto_20200819_1150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectinventory',
            name='product_out',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='out_product', to='inventories.ProductOut'),
        ),
    ]
