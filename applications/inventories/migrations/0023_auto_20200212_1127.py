# Generated by Django 3.0.2 on 2020-02-12 11:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventories', '0022_auto_20200212_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchaserequest',
            name='purchaser',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='purchase_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='PurchasePerson',
        ),
    ]
