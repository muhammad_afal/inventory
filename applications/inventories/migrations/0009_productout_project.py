# Generated by Django 3.0.2 on 2020-01-24 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventories', '0008_productout_stk_product_out'),
    ]

    operations = [
        migrations.AddField(
            model_name='productout',
            name='project',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
