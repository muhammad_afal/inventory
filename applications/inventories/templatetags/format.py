from datetime import datetime
from django import template
from applications.inventories.models import Accessories

register = template.Library()

@register.filter
def get_difference(available,required):
    print(available,required)
    return required - available

@register.filter
def adding(required,available):
    print(required,available)
    return required  + available

@register.filter
def get_number(number):
    if len(str(number)) == 1:
        return "PR-000" + str(number)
    if len(str(number)) == 2:
        return "PR-00" + str(number)
    if len(str(number)) == 3:
        return "PR-0" + str(number)
    if len(str(number)) == 4:
        return "PR-" + str(number)


@register.filter
def date_format(input):
    today = datetime.now().date()
    diff = (today - input).days
    return diff


@register.filter
def total_count(input):
    obj = Accessories.objects.get(id=input)
    total_count = 0
    for item in obj.model_names.all():
        total_count = total_count + item.count_delivered
    return total_count

@register.filter
def get_name(input):
    letters = ''
    words = input.split()
    for word in words:
        letters = letters + word[0].upper()
    return letters