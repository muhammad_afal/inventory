from django.contrib import admin
from import_export import resources, widgets
from import_export.fields import Field
from applications.accounts.models import User

from . models import Product,ProductIn,ProductOut,OverallInventory,ProductPurchase,BinLocation,InventoryReturned,\
    PurchaseRequest, Projects, Vendors, ProjectInventory, Innerholes, Shelves, ModelNames,Accessories
from import_export.admin import ImportExportModelAdmin

class ProductInResource(resources.ModelResource):
    class Meta:
        model = ProductIn
        fields = ('part_name', 'part_description', 'count', 'stk_product_in')

class InventoryResource(resources.ModelResource):

    class Meta:
        model = OverallInventory
        fields = ('part_name', 'part_description', 'product_in_count', 'product_out_count','total_count', 'bin_location', 'stk')

class ProductOutResource(resources.ModelResource):
    class Meta:
        model = ProductOut
        fields = ('part_name', 'part_description', 'count', 'stk_product_out')

class ProductPurchaseResource(resources.ModelResource):

    purchaser = Field(
        column_name='purchaser',
        attribute='purchaser',
        widget=widgets.ForeignKeyWidget(User, 'first_name'))

    location = Field(
        column_name='hole',
        attribute='location',
        widget=widgets.ForeignKeyWidget(Innerholes, 'name'))

    vendor = Field(
        column_name='vendor',
        attribute='vendor_name',
        widget=widgets.ForeignKeyWidget(Vendors, 'name'))


    class Meta:
        model = ProductPurchase
        fields = ('part_name', 'part_description', 'count', 'stk_product_out','purchased_on','purchaser')

class ProjectBom(resources.ModelResource):


    available = Field(
        column_name='available',
        attribute='available',
        widget=widgets.ForeignKeyWidget(OverallInventory, 'part_name'))

    project = Field(
        column_name='project',
        attribute='project',
        widget=widgets.ManyToManyWidget(Projects, field='name'))

    class Meta:
        model = ProjectInventory
        skip_unchanged = True
        report_skipped = True
        exclude = ('id',)
        import_id_fields = ('id',)
        fields = ('part_name', 'part_description', 'count','available','project')



class ProjectAdmin(ImportExportModelAdmin):
    resource_class = ProjectBom

class InventoryAdmin(ImportExportModelAdmin):
    resource_class = InventoryResource

class ProductInAdmin(ImportExportModelAdmin):
    resource_class = ProductInResource

class ProductOutAdmin(ImportExportModelAdmin):
    resource_class = ProductOutResource

class ProductPurchaseAdmin(ImportExportModelAdmin):
    resource_class = ProductPurchaseResource

class PurchaseRequestAdmin(admin.ModelAdmin):
    exclude = ("stk_product_in",'age','part_number',)

admin.site.register(ProductIn,ProductInAdmin)
admin.site.register(ProductOut,ProductOutAdmin)
admin.site.register(ProductPurchase, ProductPurchaseAdmin)
admin.site.register(OverallInventory,InventoryAdmin)
admin.site.register(BinLocation)
admin.site.register(InventoryReturned)
admin.site.register(PurchaseRequest,PurchaseRequestAdmin)
admin.site.register(Projects)
admin.site.register(ProjectInventory,ProjectAdmin)
admin.site.register(ModelNames)
admin.site.register(Accessories)
admin.site.register(Vendors)
admin.site.register(Innerholes)
admin.site.register(Shelves)