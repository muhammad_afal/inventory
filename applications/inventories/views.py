import json
import xlwt
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.http import HttpResponse,JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views import View
from .models import ProductIn,ProductOut, ProductPurchase,OverallInventory,File,BinLocation,InventoryReturned,\
    PurchaseRequest, Projects, Vendors, ProjectInventory, Shelves, Innerholes,ModelNames,Accessories
from applications.accounts.models import User, Teams

CHOICES = (
            ("1", 'Pending'),
            ("2", 'Order Placed'),
            ("3", 'Delivered'),
        )

def validateEmail(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False

class DashboardView(View):

    @method_decorator(login_required)
    def get(self,request):
        # product_in = ProductIn.objects.all().count()
        # product_out = ProductOut.objects.all().count()
        # purchased = ProductPurchase.objects.all().count()
        # overall = OverallInventory.objects.all().count()
        # bin_location = BinLocation.objects.all().count()
        # returned = InventoryReturned.objects.all().count()
        # requests = PurchaseRequest.objects.all().count()
        # context = {"product_in":product_in,"product_out":product_out,"purchased":purchased,"overall":overall,"bin":bin_location,"returned":returned,'requests':requests}
        return render(request, 'index.html')


class ProductInView(View):
    @method_decorator(login_required)
    def get(self,request):

        items_list = ProductIn.objects.all()
        bin_location = BinLocation.objects.all()
        context = {
            'items_list': items_list,
            'bin_locations':bin_location,
        }
        return render(request, 'new_product_in.html', context)


class ProductOutView(View):
    @method_decorator(login_required)
    def get(self,request):
        today = datetime.today().date()
        items_list = ProductOut.objects.all()
        overall = OverallInventory.objects.all()
        teams = Teams.objects.all()
        users = User.objects.all()
        mechanical_users = users.filter(team__name="Mechanical")
        electronics_users = users.filter(team__name="Electronics")
        software_users = users.filter(team__name="Software")
        production_users = users.filter(team__name="Production")
        context = {
            'items_list': items_list,
            'overall':overall,
            'today':today,
            'teams':teams,
            "mechanical_users":mechanical_users,
            "electronics_users":electronics_users,
            "software_users":software_users,
            "production_users":production_users
        }
        return render(request, 'new_product_out.html', context)


class ProductPurchasedView(View):
    @method_decorator(login_required)
    def get(self,request):
        today = datetime.today().date()
        items_list = ProductPurchase.objects.all().order_by('created')
        users = User.objects.filter(purchaser=True)
        bin_locations = BinLocation.objects.all()
        vendors = Vendors.objects.all()
        shelves = Shelves.objects.all()
        innerholes = Innerholes.objects.all()
        context = {
            'items_list': items_list,
            'today': today,
            'users':users,
            "bin_locations":bin_locations,
            "vendors":vendors,
            "shelves":shelves,
            "innerholes":innerholes

        }
        return render(request, 'new_product_purchased.html', context)

class PurchaseRequestView(View):
    @method_decorator(login_required)
    def get(self, request):
        items_list = PurchaseRequest.objects.all()
        persons = User.objects.filter(purchaser=True)
        context = {
            'items_list': items_list,
            'choices':CHOICES,
            'persons':persons,
            "projects":Projects.objects.all()
        }
        return render(request, 'new_purchase_request.html', context)


class OverallInventoryView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = OverallInventory.objects.all()
            context = {
                'items_list': items,
            }
            return render(request, 'new_overall.html', context)

class ProjectDetailView(View):
    @method_decorator(login_required)
    def get(self,request,*args,**kwargs):
            project = Projects.objects.get(id=kwargs["id"])
            overall = OverallInventory.objects.all()
            try:
                items_list = ProjectInventory.objects.filter(project=project)
                context = {"items_list":items_list,
                           "project":project,
                           "overall":overall,
                           }
                return render(request, 'project_detail.html', context)
            except:
                context = {
                           "project": project,
                            "overall": overall,
                }
                return render(request, 'project_detail.html', context)


class ReturnedView(View):
    @method_decorator(login_required)
    def get(self, request):
        today = datetime.today().date()
        products = ProductOut.objects.all()
        teams = Teams.objects.all()
        projects = Projects.objects.all()
        users = User.objects.all()
        mechanical_users = users.filter(team__name="Mechanical")
        electronics_users = users.filter(team__name="Electronics")
        software_users = users.filter(team__name="Software")
        production_users = users.filter(team__name="Production")
        items_list = InventoryReturned.objects.all()
        context = {
            'items_list': items_list,
            'today': today,
            'products':products,
            'teams':teams,
            'projects':projects,
            "mechanical_users": mechanical_users,
            "electronics_users": electronics_users,
            "software_users": software_users,
            "production_users": production_users
        }
        return render(request, 'new_returned.html', context)


class AddProductInView(View):

    def post(self,request):
        if  self.request.POST['part_name']:
            try:
                    obj = ProductIn.objects.get(id=self.request.POST["id"])
                    obj.part_description = self.request.POST['part_description']
                    obj.part_name = self.request.POST['part_name']
                    # if self.request.POST['count']:
                    #     obj.count = self.request.POST['count']
                    # if self.request.POST.get('bin_location'):
                    #     bin_location_obj = BinLocation.objects.get(name=self.request.POST['bin_location'])
                    # obj.bin_in_location = bin_location_obj
                    obj.save()
            except:
                    obj = ProductIn()
                    obj.part_description = self.request.POST['part_description']
                    obj.part_name = self.request.POST['part_name']
                    items = list(ProductIn.objects.values_list('part_name',flat=True))
                    if obj.part_name in items:
                        data = {}
                        data['result'] = "Partname Already Exist"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    # if self.request.POST['count']:
                    #     obj.count = self.request.POST['count']
                    # if self.request.POST.get('bin_location'):
                    #     bin_location_obj = BinLocation.objects.get(name=self.request.POST['bin_location'])
                    #     obj.bin_in_location = bin_location_obj
                    obj.save()
            try:
                overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
            except:
                items = list(OverallInventory.objects.values_list('part_name',
                                                                  flat=True))
                if request.POST['part_name'] in items:
                    overall_obj = OverallInventory.objects.get(part_name=self.request.POST['part_name'])
                else:
                    overall_obj = OverallInventory()
            overall_obj.part_name = obj.part_name
            overall_obj.product_in_count = obj.count
            overall_obj.part_description = obj.part_description
            overall_obj.product_in_count = int(obj.count)
            # overall_obj.bin_location = self.request.POST.get("bin_location")
            overall_obj.remarks = self.request.POST.get("remarks")
            overall_obj.total_count = overall_obj.product_in_count - overall_obj.product_out_count + overall_obj.product_purchased_count + overall_obj.product_returned_count
            overall_obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data ={}
            data['result'] = "partname is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")

class AddProductOutView(View):

    def post(self,request):
        print(self.request.POST)
        if  self.request.POST['part_name'] and self.request.POST['count'] and self.request.POST["project"] \
            and self.request.POST["team"] and self.request.POST["person"]:
            try:
                overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
                obj = ProductOut.objects.get(id=self.request.POST["id"])
                obj.part_description = self.request.POST['part_description']
                obj.part_name = self.request.POST['part_name']
                previous_count = obj.count
                obj.count = self.request.POST['count']
                if int(overall_obj.total_count) < int(obj.count):
                    data = {}
                    data['result'] = "Count you entered is greater than total count available.Available count is "+ str(overall_obj.total_count)
                    return HttpResponse(json.dumps(data),
                                        content_type="application/json")
                if self.request.POST['recieved_date']:
                    obj.recieved_date = self.request.POST['recieved_date']
                project_id = self.request.POST.get('project')
                project = Projects.objects.get(id=project_id)
                team_name = self.request.POST.get("team")
                team = Teams.objects.get(name=team_name)
                obj.project = project
                obj.team= team
                person_id = self.request.POST.get("person")
                person = User.objects.get(id=person_id)
                obj.person = person
                # obj.save()
                try:
                    inventory_obj = ProjectInventory.objects.get(part_name=self.request.POST.get('part_name'),
                                                                 project__name=project.name)
                    if inventory_obj.count < int(obj.count):
                        data = {}
                        data['result'] = "You are allocating more count than required.The total required count is " + str(inventory_obj.count)
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    obj.save()
                    inventory_obj.product_out = obj
                    inventory_obj.save()
                except:
                    data = {}
                    data['result'] = "No such item present in the BOM for this project"
                    return HttpResponse(json.dumps(data),content_type="application/json")
            except:
                    obj = ProductOut()
                    obj.part_description = self.request.POST['part_description']
                    obj.part_name = self.request.POST['part_name']
                    try:
                        overall_object = OverallInventory.objects.get(part_name=obj.part_name)
                        total_count = overall_object.total_count
                        if int(self.request.POST['count']) > total_count:
                            data = {}
                            data['result'] = "Count you entered is greater than total count available"
                            return HttpResponse(json.dumps(data),
                                                content_type="application/json")
                    except:
                        data = {}
                        data['result'] = "There is no such inventory available with you"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                    if self.request.POST.get('count'):
                        obj.count = self.request.POST['count']
                    # obj.recieved_by = self.request.POST['recieved_by']
                    if self.request.POST['recieved_date']:
                        today = datetime.today().strftime('%Y-%m-%d')
                        if self.request.POST['recieved_date'] > today:
                            data = {}
                            data['result'] = "The date you selected is greater than todays date"
                            return HttpResponse(json.dumps(data),content_type="application/json")
                        else:
                            obj.recieved_date = self.request.POST['recieved_date']
                    project_id = self.request.POST.get('project')
                    project = Projects.objects.get(id=project_id)
                    print(project.name)
                    team_name = self.request.POST.get("team")
                    team = Teams.objects.get(name=team_name)
                    obj.project = project
                    obj.team = team
                    person_id = self.request.POST.get("person")
                    person = User.objects.get(id=person_id)
                    obj.person = person
                    all_objects = ProductOut.objects.all()
                    for item in all_objects:
                        if item.part_name == obj.part_name and item.project == obj.project and str(item.recieved_date) == obj.recieved_date \
                                and item.person == obj.person:
                            data = {}
                            data['result'] = "This same person recieved the same partname for the same project on the same given date.Edit the same count"
                            return HttpResponse(json.dumps(data),content_type="application/json")
                    try:
                        inventory_obj = ProjectInventory.objects.get(part_name=self.request.POST.get('part_name'),
                                                                     project__name=project.name)
                        if inventory_obj.count < int(obj.count):
                            data = {}
                            data['result'] = "You are allocating more count than required"
                            return HttpResponse(json.dumps(data),
                                                content_type="application/json")
                        obj.save()
                        inventory_obj.product_out = obj
                        inventory_obj.save()
                    except:
                        data = {}
                        data['result'] = "There is no such item required for this project in BOM file"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")

            try:
                overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
            except:
                items = list(OverallInventory.objects.values_list('part_name', flat=True))
                if request.POST['part_name'] in items:
                    overall_obj = OverallInventory.objects.get(part_name=self.request.POST['part_name'])
                else:
                    overall_obj = OverallInventory()
                    overall_obj.part_name = obj.part_name
            out_products = ProductOut.objects.filter(part_name=self.request.POST['part_name'])
            count = 0
            for obj in out_products:
                count += obj.count
            overall_obj.part_description = obj.part_description
            overall_obj.product_out_count = int(count)
            overall_obj.total_count = overall_obj.product_in_count - overall_obj.product_out_count + overall_obj.product_purchased_count + overall_obj.product_returned_count
            overall_obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data = {}
            data['result'] = "partname,count,project,team and person is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddProductPurchasedView(View):

    def post(self, request):
        old_part_name = self.request.POST.get("old_part_name")
        part_name = self.request.POST.get("part_name")
        old_part_description = self.request.POST.get("old_part_description")
        part_description = self.request.POST.get("part_description")
        if self.request.POST['part_name'] and self.request.POST['count'] and self.request.POST["part_description"] and \
            self.request.POST["hole"]:
            try:
                obj = ProductPurchase.objects.get(id=self.request.POST["id"])
                obj.part_description = self.request.POST['part_description']
                obj.part_name = self.request.POST['part_name']
                if self.request.POST.get('count'):
                    obj.count = self.request.POST['count']
                vendor_id = self.request.POST['vendor']
                vendor = Vendors.objects.get(id=vendor_id)
                obj.vendor_name = vendor
                hole_id = self.request.POST.get("hole")
                hole = Innerholes.objects.get(id=hole_id)
                obj.location = hole
                if self.request.POST['purchased_on']:
                    obj.purchased_on = self.request.POST['purchased_on']
                purchaser_id = self.request.POST.get('purchaser')
                purchaser = User.objects.get(id=purchaser_id)
                obj.purchaser = purchaser
                obj.save()
            except:
                obj = ProductPurchase()
                obj.part_description = self.request.POST['part_description']
                obj.part_name = self.request.POST['part_name']
                if self.request.POST.get('count'):
                    obj.count = self.request.POST['count']
                purchaser_id = self.request.POST.get('purchaser')
                purchaser = User.objects.get(id=purchaser_id)
                obj.purchaser = purchaser
                vendor_id = self.request.POST['vendor']
                vendor = Vendors.objects.get(id=vendor_id)
                obj.vendor_name = vendor
                hole = Innerholes.objects.get(id=request.POST["hole"])
                obj.location = hole
                if self.request.POST['purchased_on']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    if self.request.POST['purchased_on'] > today:
                        data = {}
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                    else:
                        obj.purchased_on = self.request.POST['purchased_on']
                obj.save()
            try:
                overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
            except:
                items = list(OverallInventory.objects.values_list('part_name', flat=True))
                if request.POST['part_name'] in items:
                    overall_obj = OverallInventory.objects.get(part_name=self.request.POST['part_name'])
                else:
                    overall_obj = OverallInventory()
            purchased = ProductPurchase.objects.filter(part_name=self.request.POST['part_name'])
            count = 0
            for obj in purchased:
                count += obj.count
            overall_obj.part_name = obj.part_name
            overall_obj.part_description = obj.part_description
            overall_obj.product_purchased_count = int(count)
            overall_obj.location = hole
            overall_obj.total_count = overall_obj.product_in_count - overall_obj.product_out_count + overall_obj.product_purchased_count + overall_obj.product_returned_count
            overall_obj.save()
            project_inventories = ProjectInventory.objects.filter(part_name=request.POST['part_name'])
            print(project_inventories)
            for obj in project_inventories:
                obj.available = overall_obj
                obj.save()
            if not part_name == old_part_name or not old_part_description == part_description:
                try:
                    out_objs = ProductOut.objects.filter(Q(part_name=self.request.POST.get("old_part_name"))| Q(part_description=self.request.POST.get("old_part_description")))
                    for obj in out_objs:
                        obj.part_name = self.request.POST.get("part_name")
                        obj.part_description = self.request.POST.get("part_description")
                        obj.save()
                except:
                    pass
                try:
                    return_objs = InventoryReturned.objects.filter(Q(part_name=self.request.POST.get("old_part_name"))| Q(part_description=self.request.POST.get("old_part_description")))
                    for obj in return_objs:
                        obj.part_name = self.request.POST.get("part_name")
                        obj.part_description = self.request.POST.get("part_description")
                        obj.save()
                except:
                    pass
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data = {}
            data['result'] = "partname,part_description,count,shelf,box and hole is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddInventoryReturnView(View):
    def post(self, request):
        print(self.request.POST)
        if self.request.POST.get('part_name') and self.request.POST.get('count') and self.request.POST.get("project") \
        and self.request.POST.get("team"):
            try:
                obj = InventoryReturned.objects.get(id=self.request.POST["id"])
                obj.part_description = self.request.POST['part_description']
                obj.part_name = self.request.POST['part_name']
                team_name = self.request.POST["team"]
                project_name = self.request.POST["project"]
                person_id = self.request.POST.get("person")
                user = User.objects.get(id=person_id)
                if self.request.POST.get('count'):
                    obj.count_returned = self.request.POST['count']
                # obj.returned_by = self.request.POST['returned_by']
                # obj.project = self.request.POST.get('project')
                try:
                    out_obj = ProductOut.objects.get(part_name=obj.part_name,team__name=team_name,
                                                     project__name=project_name,person__first_name=user.first_name)
                    if out_obj.count < int(obj.count_returned):
                        data = {}
                        data['result'] = "The count you are returning is greater than count you recieved"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                    # count_previously_returned = self.request.POST.get("count_previous")
                    # count_now_returned = obj.count_returned
                    # if int(count_now_returned) + int(count_previously_returned) > out_obj.count:
                    #     data = {}
                    #     data['result'] = "You are returning more than what you have recieved"
                    #     return HttpResponse(json.dumps(data), content_type="application/json")

                    # else:
                    #     out_obj.count = out_obj.count - int(obj.count_returned)
                    #     print(out_obj.count, "ccccccccccccccc")
                    #     out_obj.save()
                except:
                    pass
                if self.request.POST['returned_date']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    date = out_obj.recieved_date.strftime('%Y-%m-%d')
                    if self.request.POST['returned_date'] > today:
                        data = {}
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    if self.request.POST['returned_date'] < date:
                        data = {}
                        data['result'] = "The date you selected is lesser than date you transferred this inventory"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    else:
                        obj.returned_date = self.request.POST['returned_date']
                obj.out_object = out_obj
                obj.save()
            except:
                obj = InventoryReturned()
                items = ProductOut.objects.all()
                team_name = self.request.POST.get("team")
                project_name = self.request.POST.get("project")
                person_id = self.request.POST.get("person")
                team = Teams.objects.get(name=team_name)
                project = Projects.objects.get(name=project_name)
                person = User.objects.get(id=person_id)
                part_names = items.values_list("part_name",flat=True)
                projects = items.values_list("project__name",flat=True)
                teams = items.values_list("team__name",flat=True)
                persons = items.values_list("person__first_name", flat=True)
                # recieved_persons = items.values_list("recieved_by",flat=True)
                obj.part_description = self.request.POST.get('part_description')
                obj.part_name = self.request.POST.get('part_name')
                if self.request.POST.get('count'):
                    obj.count_returned = self.request.POST['count']
                # obj.returned_by = self.request.POST['returned_by']
                obj.project = project
                obj.team = team
                obj.person = person
                try:
                    out_obj = ProductOut.objects.get(part_name=obj.part_name,team__name=team_name,
                                                     project__name=project_name,person__first_name=person.first_name)
                    if out_obj.count < int(obj.count_returned):
                        data = {}
                        data['result'] = "The count you are returning is greater than count you recieved"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                    # else:
                    #     out_obj.count = out_obj.count - int(obj.count_returned)
                    #     print(out_obj.count,"ccccccccccccccc")
                    #     out_obj.save()
                except:
                    pass
                if self.request.POST['returned_date']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    date = out_obj.recieved_date.strftime('%Y-%m-%d')
                    if self.request.POST['returned_date'] > today:
                        data = {}
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                    if self.request.POST['returned_date'] < date:
                            data = {}
                            data['result'] = "The date you selected is lesser than date you transferred this inventory"
                            return HttpResponse(json.dumps(data),
                                                content_type="application/json")
                    obj.returned_date = self.request.POST['returned_date']
                if not obj.part_name in part_names or not obj.project.name in projects or not obj.team.name in teams or \
                    obj.person in persons:
                    data = {}
                    data['result'] = "There is no such item out from you for this project to this person.Check allocated items"
                    return HttpResponse(json.dumps(data),content_type="application/json")
                returned_items = InventoryReturned.objects.all()
                for item in returned_items:
                    if obj.part_name == item.part_name and project_name == item.project.name and team_name == item.team.name and obj.returned_date == str(item.returned_date) \
                        and person.first_name == item.person.first_name:
                        data = {}
                        data['result'] = "This same person returned same item from same project on same day.Add the count with same item"
                        return HttpResponse(json.dumps(data),content_type="application/json")
                obj.out_object = out_obj
                obj.save()
            try:
                overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
            except:
                items = list(OverallInventory.objects.values_list('part_name', flat=True))
                if request.POST['part_name'] in items:
                    overall_obj = OverallInventory.objects.get(part_name=self.request.POST['part_name'])
                else:
                    overall_obj = OverallInventory()
            returned = InventoryReturned.objects.filter(part_name=self.request.POST['part_name'])
            count = 0
            for obj in returned:
                count += obj.count_returned
            overall_obj.part_name = obj.part_name
            overall_obj.part_description = obj.part_description
            overall_obj.product_returned_count = int(count)
            # overall_obj.product_out_count = overall_obj.product_out_count - int(count)
            overall_obj.total_count = overall_obj.product_in_count - overall_obj.product_out_count + overall_obj.product_purchased_count + overall_obj.product_returned_count
            overall_obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data = {}
            data['result'] = "partname,count,project and team is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddPurchaseRequestView(View):

    def post(self,request):
        obj = PurchaseRequest()
        if self.request.POST["part_name"] and self.request.POST["email"] and self.request.POST['count'] and self.request.POST["project"]:
            obj.part_name = self.request.POST["part_name"]
            obj.part_description = self.request.POST["part_description"]
            email = validateEmail(self.request.POST["email"])
            if email == True:
                obj.requested_by = self.request.POST["email"]
            else:
                data = {}
                data['result'] = "Please enter a valid email address"
                return HttpResponse(json.dumps(data),content_type="application/json")
            obj.count = self.request.POST["count"]
            project = self.request.POST["project"]
            project_obj = Projects.objects.get(id=project)
            obj.project = project_obj
            if not self.request.POST.get("purchaser"):
                data = {}
                data['result'] = "You must select a purchaser to continue.Contact administrator if no purchasers available"
                return HttpResponse(json.dumps(data),content_type="application/json")
            try:
                purchaser = self.request.POST.get("purchaser")
                user_obj = User.objects.get(email=purchaser)
                obj.purchaser = user_obj
            except:
                pass
            today = datetime.today().strftime('%Y-%m-%d')
            obj.requested_on = today
            obj.status = "1"
            obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data),content_type="application/json")
        else:
            data = {}
            data['result'] = "part_name,email and count is mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")

class AddBomView(View):

    def post(self,request):
        print(self.request.POST)
        part_name = self.request.POST.get("part_name")
        part_description = self.request.POST.get("part_description")
        count = self.request.POST.get("count")
        if part_name and part_description and count:
            try:
                id = self.request.POST.get("id")
                obj = ProjectInventory.objects.get(id=id)
                obj.count = count
                obj.save()
                data = {}
                data['result'] = "success"
                return HttpResponse(json.dumps(data), content_type="application/json")
            except:
                obj = ProjectInventory()
                obj.part_name = part_name
                obj.part_description = part_description
                obj.count = count
                try:
                    overall_obj = OverallInventory.objects.get(part_name=part_name)
                    obj.available = overall_obj
                except:
                    pass
                obj.save()
                project = Projects.objects.get(name=self.request.POST.get("project"))
                obj.project.add(project)
                data = {}
                data['result'] = "success"
                return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = {}
            data['result'] = "part_name,part_description and count is mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")

class DeleteProductInView(View):
        @method_decorator(login_required)
        def get(self, form):
            id = self.request.GET.get("id")
            obj = ProductIn.objects.get(id=id)
            count = int(obj.count)
            part_name = obj.part_name
            try:
                ovr_obj = OverallInventory.objects.get(part_name=part_name)
                if ovr_obj.product_in_count > 0:
                    ovr_obj.product_in_count = ovr_obj.product_in_count - count
                ovr_obj.remarks = ''
                ovr_obj.bin_location = ''
                ovr_obj.total_count = ovr_obj.product_in_count - ovr_obj.product_out_count + ovr_obj.product_purchased_count + ovr_obj.product_returned_count
                ovr_obj.save()
            except:
                pass
            obj.delete()
            return JsonResponse(
                {"success": "Product Deleted Successfully"})


class DeleteProductOutView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = ProductOut.objects.get(id=id)
        count = int(obj.count)
        returned = obj.product_out.all()
        for item in returned:
            count_returned = item.count_returned
        part_name = obj.part_name
        try:
            ovr_obj = OverallInventory.objects.get(part_name=part_name)
            if ovr_obj.product_returned_count <= 0:
                ovr_obj.product_out_count = ovr_obj.product_out_count - count
                ovr_obj.total_count = ovr_obj.total_count + count
            elif ovr_obj.product_returned_count > 0:
                ovr_obj.product_out_count = ovr_obj.product_out_count - count
                ovr_obj.product_returned_count = ovr_obj.product_returned_count - count_returned
            # ovr_obj.total_count = ovr_obj.product_in_count - ovr_obj.product_out_count + ovr_obj.product_purchased_count + ovr_obj.product_returned_count - ovr_obj.total_count
            ovr_obj.total_count =  ovr_obj.product_purchased_count - ovr_obj.product_out_count + ovr_obj.product_returned_count
            if ovr_obj.total_count < 0:
                ovr_obj.total_count = ovr_obj.product_purchased_count;
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse({"success": "Product Deleted Successfully"})


class DeleteProductPurchasedView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = ProductPurchase.objects.get(id=id)
        count = int(obj.count)
        part_name = obj.part_name
        try:
            ovr_obj = OverallInventory.objects.get(part_name=part_name)
            if ovr_obj.product_purchased_count > 0:
                ovr_obj.product_purchased_count = ovr_obj.product_purchased_count - count
            ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_in_count - ovr_obj.product_out_count + ovr_obj.product_returned_count
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteBoxView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = BinLocation.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteHoleView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = Innerholes.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteProjectView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = Projects.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteModelView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = ModelNames.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteAccessoryView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = Accessories.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteShelfView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = Shelves.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})

class DeleteInventoryReturnView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = InventoryReturned.objects.get(id=id)
        count = int(obj.count_returned)
        part_name = obj.part_name
        try:
            ovr_obj = OverallInventory.objects.get(part_name=part_name)
            if ovr_obj.product_returned_count > 0:
                ovr_obj.product_returned_count = ovr_obj.product_returned_count - count
            ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_in_count - ovr_obj.product_out_count +  + ovr_obj.product_returned_count
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})

class DeleteFromBomListView(View):
    @method_decorator(login_required)
    def get(self, form):
        id = self.request.GET.get("id")
        obj = ProjectInventory.objects.get(id=id)
        count = int(obj.count)
        # part_name = obj.part_name
        # try:
        #     ovr_obj = OverallInventory.objects.get(part_name=part_name)
        #     if ovr_obj.product_purchased_count > 0:
        #         ovr_obj.product_purchased_count = ovr_obj.product_purchased_count - count
        #     ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_in_count - ovr_obj.product_out_count + ovr_obj.product_returned_count
        #     ovr_obj.save()
        # except:
        #     pass
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


def export_xls_in(request):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="inventory.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Users')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        font_style.num_format_str = 'dd/mm/yyyy'

        columns = ['Part Name', 'Part Description', 'Count', ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()
        font_style.num_format_str = 'dd/mm/yyyy'

        rows = ProductIn.objects.all().values_list('part_name', 'part_description', 'count')
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


def export_xls_out(request):
        response = HttpResponse(content_type='application/ms-excel')
        response[
            'Content-Disposition'] = 'attachment; filename="inventory_out.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Users')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        font_style.num_format_str = 'dd/mm/yyyy'

        columns = ['Part Name', 'Part Description', 'Count','recieved_by','recieved_date','project',]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()
        font_style.num_format_str = 'dd/mm/yyyy'

        rows = ProductOut.objects.all().values_list('part_name','part_description', 'count',"recieved_by",'recieved_date','project',)
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


def export_xls_purchased(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_purchased.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.num_format_str = 'dd/mm/yyyy'

    columns = ['Part Name', 'Part Description', 'Count', 'vendor',
               'purchaser', 'purchased_on', ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = 'dd/mm/yyyy'

    rows = ProductPurchase.objects.all().values_list('part_name',
                                                'part_description', 'count',
                                                 'vendor',"purchaser", 'purchased_on')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

def export_xls_returned(request):
        response = HttpResponse(content_type='application/ms-excel')
        response[
            'Content-Disposition'] = 'attachment; filename="inventory_returned.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Users')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        font_style.num_format_str = 'dd/mm/yyyy'

        columns = ['part_name','part_description','count_returned', 'returned_by','returned_date','project', ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()
        font_style.num_format_str = 'dd/mm/yyyy'

        rows = InventoryReturned.objects.all().values_list('part_name',
                                                    'part_description',
                                                    'count_returned', 'returned_by',
                                                    'returned_date',
                                                    'project', )
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


def export_xls_overall(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_returned.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.num_format_str = 'dd/mm/yyyy'

    columns = ['part_name', 'part_description', 'total-count',
               'bin_location', 'remarks',]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = 'dd/mm/yyyy'

    rows = OverallInventory.objects.all().values_list('part_name',
                                                       'part_description',
                                                       'total_count',
                                                       'bin_location',
                                                       'remarks',
                                                        )
    print(rows)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def export_xls_purchase_request(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="purchase_request.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.num_format_str = 'dd/mm/yyyy'

    columns = ['part_name', 'part_description', 'request-no','count'
               'requested_by', 'requested_on','status' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = 'dd/mm/yyyy'

    rows = PurchaseRequest.objects.all().values_list('part_name',
                                                      'part_description',
                                                      'request_number',
                                                        'count',
                                                      'requested_by',
                                                      'requested_on',
                                                     'status',
                                                      )
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

def export_xls_bom_list(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename=' + request.GET.get("project") + 'BoM.xls'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'

    columns = ['part_name', 'part_description', 'required-count','available-count' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'


    rows = ProjectInventory.objects.filter(project__name=request.GET.get("project")).values_list('part_name',
                                                      'part_description',
                                                        'count',
                                                      'available__total_count',
                                                      )
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response



class ProjectView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = Projects.objects.all()
            context = {
                'items_list': items,
            }
            return render(request, 'projects.html', context)


class ModelView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = ModelNames.objects.all()
            accessories = Accessories.objects.all()
            today = datetime.today()
            print(items)
            context = {
                'items_list': items,
                'accessories':accessories,
                'today':today
            }
            return render(request, 'models.html', context)


class AccessoryView(View):
    @method_decorator(login_required)
    def get(self,request):
            accessories = Accessories.objects.all()
            models = ModelNames.objects.all()
            print(accessories)
            today = datetime.today()
            context = {
                'accessories':accessories,
                'today':today,
                'models':models
            }
            return render(request, 'accessories.html', context)


class ShelvesView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = Shelves.objects.all()
            context = {
                'items_list': items,
            }
            return render(request, 'shelves.html', context)


class BoxesView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = BinLocation.objects.all()
            context = {
                'items_list': items,
            }
            return render(request, 'boxes.html', context)

class HolesView(View):
    @method_decorator(login_required)
    def get(self,request):
            items = Innerholes.objects.all()
            bins = BinLocation.objects.all()
            shelves = Shelves.objects.all()
            context = {
                'items_list': items,
                'bins':bins,
                'shelves':shelves
            }
            return render(request, 'holes.html', context)


class AddBoxView(View):
    def post(self, request):
        try:
            bin_obj = BinLocation.objects.get(id=self.request.POST.get("id"))
        except:
            bin_obj = BinLocation()
        name = self.request.POST.get('box_name')
        if not name:
            data = {}
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        bin_obj.name = name
        bin_obj.save()
        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),content_type="application/json")


class AddHoleView(View):
    def post(self, request):
        print(self.request.POST)
        try:
            hole = Innerholes.objects.get(id=self.request.POST.get("id"))
        except:
            hole = Innerholes()
        name = self.request.POST.get('hole-name')
        box = BinLocation.objects.get(id=request.POST.get("box-name"))
        shelf = Shelves.objects.get(id=request.POST.get("shelf-name"))
        if not name or not self.request.POST["shelf-name"] or not self.request.POST['box-name']:
            data = {}
            data['result'] = "Please Fill The Fields"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        hole.name = name
        hole.bin_obj = box
        hole.shelf = shelf
        hole.save()
        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),content_type="application/json")


class AddProjectView(View):
    def post(self, request):
        try:
            proj_obj = Projects.objects.get(id=self.request.POST.get("id"))
        except:
            proj_obj = Projects()
        name = self.request.POST.get('project_name')
        if not name:
            data = {}
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        proj_obj.name = name
        proj_obj.save()
        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),content_type="application/json")


class AddModelView(View):
    def post(self, request):
        if self.request.POST.get("model_name") and self.request.POST.get("count_delivered"):
            try:
                model_obj = ModelNames.objects.get(id=self.request.POST.get("id"))
            except:
                model_obj = ModelNames()
            model_obj.model_name = self.request.POST.get('model_name')
            model_obj.count_delivered = self.request.POST.get('count_delivered')
            model_obj.date_of_manufacture = self.request.POST.get('date_of_manufacture')
            model_obj.remarks = self.request.POST.get('remarks')
            model_obj.item_code = self.request.POST.get('item_code')
            model_obj.voltage = self.request.POST.get("voltage")
            model_obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = {}
            data['result'] = "Model name and count delivered is Mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")


class AddAccessoryView(View):
    def post(self, request):
        print(self.request.POST)
        if self.request.POST.get("name") and self.request.POST.get("item_code") and self.request.POST.getlist("models"):
            try:
                obj = Accessories.objects.get(id=self.request.POST.get("id"))
            except:
                obj = Accessories()
            obj.name = self.request.POST.get('name')
            obj.item_code = self.request.POST.get('item_code')
            models = self.request.POST.getlist('models')
            try:
                current_models = obj.model_names.all()
                for model in current_models:
                    obj.model_names.remove(model)
            except:
                pass
            for model in models:
                obj.model_names.add(model)
            obj.save()
            data = {}
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = {}
            data['result'] = "Model name and count delivered is Mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")


class AddShelfView(View):
    def post(self, request):
        try:
            proj_obj = Shelves.objects.get(id=self.request.POST.get("id"))
        except:
            proj_obj = Shelves()
        name = self.request.POST.get('shelf_name')
        if not name:
            data = {}
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        proj_obj.name = name
        proj_obj.save()
        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),content_type="application/json")


class ChangeStatusView(View):
    def post(self,request):
        print(self.request.POST)
        obj = PurchaseRequest.objects.get(id=self.request.POST['id'])
        obj.status = self.request.POST.get('status')
        obj.save()
        message = render_to_string('request_confirmation_mail.html', {
            'part_name': obj.part_name,
            'part_description': obj.part_description,
            'count': obj.count,
            'request_no': obj.request_number,
            "requested_on": obj.requested_on,
            "status": obj.get_status_display(),
            "project": obj.project,
            "requested_by": obj.requested_by,
        })
        to_email = obj.requested_by
        send_mail(
            'Status Changed.',
            'RandD Inventory',
            '147444@ust-global.com',
            [to_email],
            html_message=message, )

        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),content_type="application/json")



class ModelDetailView(View):
    def get(self, request,**kwargs):
        obj = ModelNames.objects.get(id=kwargs["id"])
        print(obj)
        accessories = Accessories.objects.filter(model_names=obj)
        print(accessories)
        table_headings = []
        for i in range(1,obj.count_delivered+1):
            table_headings.append(obj.item_code+str(i).zfill(4))
        print(table_headings)
        context = {
            'accessories': accessories,
            "model":obj,
            "headings":table_headings
        }
        return render(request, 'model-detail.html', context)

class AccessoryDetailView(View):
    def get(self, request,**kwargs):
        obj = Accessories.objects.get(id=kwargs["id"])
        print(obj)
        context = {
            "accessory":obj,
        }
        return render(request, 'accessory-detail.html', context)